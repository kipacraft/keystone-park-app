//
//  Student+CoreDataProperties.swift
//  Keystone Park
//
//  Created by Daniel Nimafa on 17/11/18.
//  Copyright © 2018 Kipacraft. All rights reserved.
//
//

import Foundation
import CoreData


extension Student {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Student> {
        return NSFetchRequest<Student>(entityName: "Student")
    }

    @NSManaged public var name: String?
    @NSManaged public var lesson: Lesson?

}
