//
//  LessonVCTableViewController.swift
//  Keystone Park
//
//  Created by Daniel Nimafa on 18/11/18.
//  Copyright © 2018 Kipacraft. All rights reserved.
//

import UIKit
import CoreData

class LessonVC: UITableViewController {
    
    // MARK: - Public Properties
    var moc: NSManagedObjectContext? {
        didSet {
            if let managedContext = moc {
                lessonService = LessonService(managedContext)
            }
        }
    }
    
    // MARK: - Private Properties
    private var studentList = [Student]()
    var lessonService: LessonService?
    private var studentToUpdate: Student?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loadStudents()
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return studentList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "studentCell", for: indexPath)
        cell.textLabel?.text = studentList[indexPath.row].name
        cell.detailTextLabel?.text = studentList[indexPath.row].lesson?.type
        cell.detailTextLabel?.textColor = #colorLiteral(red: 0.501960814, green: 0.501960814, blue: 0.501960814, alpha: 1)

        return cell
    }
    
    //MARK: - TableView delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        studentToUpdate = studentList[indexPath.row]
        present(addController(actionType: "update"), animated: true, completion: nil)
    }
    
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            lessonService?.delete(student: studentList[indexPath.row])
            studentList.remove(at: indexPath.row)
            tableView.deleteRows(at: [indexPath], with: .fade)
        }
        
        tableView.reloadData()
    }

    // MARK: - IBActions
    @IBAction func AddStudentBtnPressed(_ sender: UIBarButtonItem) {
        present(addController(actionType: "add"), animated: true, completion: nil)
    }
    
    // MARK: - Private functions
    private func addController(actionType: String) -> UIAlertController {
        let alertController = UIAlertController(title: "Keystone Park", message: "Student Info", preferredStyle: .alert)
        
        alertController.addTextField { [weak self] (textField: UITextField) in
            textField.placeholder = "Name"
            textField.text = self?.studentToUpdate == nil ? "" : self?.studentToUpdate?.name
        }
        
        alertController.addTextField { [weak self] (textField: UITextField) in
            textField.placeholder = "Lesson type: Ski | Snowboard"
            textField.text = self?.studentToUpdate == nil ? "" : self?.studentToUpdate?.lesson?.type
        }
        
        let defaultAction = UIAlertAction(title: actionType.uppercased(), style: .default) { (action) in
            guard let studentName = alertController.textFields?[0].text, let lesson = alertController.textFields?[1].text else { return }
            
            if actionType.caseInsensitiveCompare("add") == .orderedSame {
                if let lessonType = LessonType(rawValue: lesson.lowercased()) {
                    self.lessonService?.addStudent(name: studentName, for: lessonType, completion: { (success, students) in
                        if success {
                            self.studentList = students
                        }
                    })
                }
            } else {
                guard let name = alertController.textFields?.first?.text, !name.isEmpty,
                let studentToUpdate = self.studentToUpdate,
                let lessonType = alertController.textFields?[1].text
                    else { return }
                
                self.lessonService?.updateStudent(currentStudent: studentToUpdate, withName: name, forLesson: lessonType)
                self.studentToUpdate = nil
            }
            
            DispatchQueue.main.async {
                self.loadStudents()
            }
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .default) { [weak self] (action) in
            self?.studentToUpdate = nil
        }
        
        alertController.addAction(defaultAction)
        alertController.addAction(cancelAction)
        
        return alertController
    }
    
    private func loadStudents() {
        if let result = lessonService?.getAllStudents() {
            studentList = result
            tableView.reloadData()
        }
    }

}
