//
//  LessonService.swift
//  Keystone Park
//
//  Created by Daniel Nimafa on 18/11/18.
//  Copyright © 2018 Kipacraft. All rights reserved.
//

import Foundation
import CoreData

enum LessonType: String {
    case ski, snowboard
}

typealias StudentHandler = (Bool, [Student]) -> ()

class LessonService {
    private let moc: NSManagedObjectContext
    private var students = [Student]()
    
    init(_ moc: NSManagedObjectContext) {
        self.moc = moc
    }
    
    // MARK: - Public
    
    // READ
    func getAllStudents() -> [Student]? {
        
        let sortByLesson = NSSortDescriptor(key: "lesson.type", ascending: true)
        let sortByName = NSSortDescriptor(key: "name", ascending: true)
        let sortDescriptors = [sortByLesson, sortByName]
        
        let number = 10
        
        let request: NSFetchRequest<Student> = Student.fetchRequest()
        request.sortDescriptors = sortDescriptors
        
        do {
            students = try moc.fetch(request)
            return students
        }
        catch let error {
            print("Failed to get all students: \(error.localizedDescription)")
        }
        
        return nil
    }
    
    // WRITE
    func addStudent(name: String, for type: LessonType, completion: StudentHandler) {
        let student = Student(context: moc)
        student.name = name
        
        if let lesson = lessonExist(type) {
            register(student, for: lesson)
            students.append(student)
            
            completion(true, students)
        }
        
        save()
    }
    
    // UPDATE
    func updateStudent(currentStudent student: Student, withName name: String, forLesson lesson: String) {
        if student.lesson?.type?.caseInsensitiveCompare(lesson) == .orderedSame {
            let lesson = student.lesson
            let studentList = Array(lesson?.students?.mutableCopy() as! NSMutableSet) as! [Student]
            
            if let index = studentList.index(where: { $0 == student }) {
                studentList[index].name = name
                lesson?.students = NSSet(array: studentList)
            }
        } else {
            if let lesson = lessonExist(LessonType(rawValue: lesson)!) {
                lesson.removeFromStudents(student)
                
                student.name = name
                register(student, for: lesson)
            }
        }
        
        save()
    }
    
    // DELETE
    func delete(student: Student) {
        let lesson = student.lesson
        
        students = students.filter({ $0 != student })
        lesson?.removeFromStudents(student)
        moc.delete(student)
        save()
    }
    
    // MARK: - Private
    private func lessonExist(_ type: LessonType) -> Lesson? {
        let request: NSFetchRequest<Lesson> = Lesson.fetchRequest()
        request.predicate = NSPredicate(format: "type = %@", type.rawValue)
        
        var lesson: Lesson?
        
        do {
            let result = try moc.fetch(request)
            lesson = result.isEmpty ? addNew(lesson: type) : result.first
        } catch let err as NSError {
            print("Error fetching lesson: \(err.localizedDescription)")
        }
        
        return lesson
    }
    
    private func addNew(lesson type: LessonType) -> Lesson {
        let lesson = Lesson(context: moc)
        lesson.type = type.rawValue
        return lesson
    }
    
    private func register(_ student: Student, for lesson: Lesson) {
        student.lesson = lesson
    }
    
    private func save() {
        do {
            try moc.save()
        } catch let error {
            print("Failed to save data: \(error.localizedDescription)")
        }
    }
}
